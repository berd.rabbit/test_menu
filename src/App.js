import React from "react";
import { useState } from 'react';

function App() {
    
    const [frontPage,setVisible]=useState("flex flex-col p-0 mt-[16px]");
    const [menuPage,setMenu]=useState("hidden");
    function ToggleMenu(){

        if (frontPage==="hidden") {
            setVisible("flex flex-col p-0 mt-[16px]");
            setMenu("hidden");
        }
        if (frontPage==="flex flex-col p-0 mt-[16px]") {
                setVisible("hidden");
                setMenu("h-[calc(100vh-72px)] bg-gray-900");
           
        }
            
    }

    

   
    return <div className="flex flex-col p-0
    bg-gray-100" >

        <div className="flex pl-[8px] pr-[28px] justify-between items-center   h-[72px] bg-gray-900" >
        <img  className=" h-[30px]" src={require('../img/Logo.png')} /> 
        <img  className=" h-[16px]" src={require('../img/search.png')} /> 
        <img  className=" h-[16px]" src={require('../img/Language.png')} /> 
        <img onClick={ToggleMenu} 
            className=" h-[16px]" src={require('../img/bars.png')} /> 


           
        </div>

        <div className={menuPage}>
            <div className=" m-[8px] rounded-lg bg-gray-800 flex justify-center items-center p-[8px] gap-[8px]">
            <img src={require('../img/circle-user.png')}/>
            <img src={require('../img/Date.png')}/>
            </div>
            <div className="flex flex-col items-start m-[8px]  ">
                <div className="flex border-b  border-white w-[100%] items-center pl-[4px] h-[56px]">
                    <img  src={require('../img/Text1.png')}/> 
                </div>
                <div className="flex border-b  border-white w-[100%] items-center pl-[4px] h-[56px]">
                    <img  src={require('../img/Text2.png')}/> 
                </div>
                <div className="flex  w-[100%] items-center pl-[4px] h-[56px]">
                    <img  src={require('../img/Text3.png')}/> 
                </div>

                

            </div>
            

        </div>

        <div className={frontPage}>
        
            <div className="flex flex-col items-start h-[173px]  p-0 mb-[8px]  ">
             
                <div className="flex flex-row items-center py-[4px] px-[0px]">
                <p className="flex items-center text-gray-900 font-sans font-medium
                    font-bold text-[18px] not-italic leading-[24px] px-[8px]"> Recommended for you</p>
                </div>

                <div  className="max-w-full min-w-full">
                    <div className="flex flex-col items-start justify-center 
                    p-0 overflow-x-scroll 
                    ">

                        <div className="flex flex-row items-start  mx-[8px]
                        p-0 gap-[16px] ">

                            <div className="card">
                                <div className="imgDiv">
                                    <img  className=" h-[30px]" src={require('../img/obi.png')} /> 
                                </div>
                                <p className="imgTitle">ОБИ</p>
                            </div>

                            <div className="card">
                                <div className="imgDiv">
                                        <img className=" h-[70px]"  src={require('../img/ler.png')} /> 
                                </div>
                                    
                                    <p className="imgTitle">Леруа <br/> Мерлен</p>
                            </div>

                            <div className="card">
                                <div className="imgDiv">
                                        <img className=" h-[70px]"  src={require('../img/cat1.png')} /> 
                                </div>
                                    
                                    <p className="imgTitle">Кот съел рекомендацию</p>
                            </div>

                            <div className="card">
                                <div className="imgDiv">
                                        <img className=" h-[70px]"  src={require('../img/cat2.png')} /> 
                                    </div>
                                    
                                    <p className="imgTitle">Курсы пения для котов</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div className="h-[100px] mt-[8px] p-[8px]  mx-[8px] rounded-lg
            bg-gradient-to-b from-[#CBD623] to-[#41701E] gap-3
            flex flex-row justify-center items-start">
                <div className="flex flex-col justify-center items-start  gap-1 w-[70vw] h-[84px]">
                    <p className="text-[#FFF] font-sans font-semibold
                    text-sm not-italic leading-5 text-center ">Rubber-bearing ficus (elastic)</p>
                    <p className=" text-[#FFF] font-sans font-normal 
                    text-sm not-italic leading-5 text-center text-opacity-50">Ficus elastica (Ficus elastica), or rubber-bearing ficus...</p>
                </div>
                <img className="w-[20vw] max-w-[50px]" src={require('../img/ficus.png')}></img>

            </div>

            <div className="h-[466px] p-0 
            mx-[8px]  mt-[16px]   flex flex-col justify-center items-start gap-2">

                <div className=" w-[100%]   flex flex-row items-center justify-between h-[40px] py-[4px] px-[0px]">
                    <p className="text-gray-900 font-sans font-bold text-lg not-italic leading-6 ">Plants and flowers</p>

                    <a className="text-[#6BA91A] font-sans font-medium text-base not-italic leading-5">See all</a>
                </div>

                <div className="w-[100%] gap-2 grid grid-cols-2 ">
                    <div className="plantBox">
                        <div className="plantImgDiv">
                            <img className="h-20" src={require('../img/1.png')}/> 
                        </div> 
                        <div className="rating">
                            <img className=" h-3" src={require('../img/Star.png')}/> 
                             <p className="text-xs text-white rounded-sm">
                                 8.0
                            </p>    
                        </div>
                        <div className="w-full flex flex-col items-center justify-end p-[8px] gap-5">
                            <p className="text-gray-900 font-sans font-medium 
                            text-base not-italic leading-5 text-center">
                            Orchid
                            </p>
                            <div className="plantButtonDiv">
                                <p className="plantButtonText">
                                Review
                                </p>

                            </div>
                            
                        </div>

                    </div>

                    <div className="plantBox">
                        <div className="plantImgDiv">
                            <img className="h-20" src={require('../img/2.png')}/> 
                        </div> 
                        <div className="rating">
                            <img className=" h-3" src={require('../img/Star.png')}/> 
                             <p className="text-xs text-white rounded-sm">
                                 8.0
                            </p>    
                        </div>
                        <div className="w-full flex flex-col items-center justify-end p-[8px] gap-5">
                            <p className="text-gray-900 font-sans font-medium 
                            text-base not-italic leading-5 text-center">
                            Pions
                            </p>
                            <div className="plantButtonDiv">
                                <p className="plantButtonText">
                                Review
                                </p>

                            </div>
                            
                        </div>

                    </div>

                    <div className="plantBox">
                        <div className="plantImgDiv">
                            <img className="h-20" src={require('../img/3.png')}/> 
                        </div> 
                        <div className="rating">
                            <img className=" h-3" src={require('../img/Star.png')}/> 
                             <p className="text-xs text-white rounded-sm">
                                 8.0
                            </p>    
                        </div>
                        <div className="w-full flex flex-col items-center justify-end p-[8px] gap-5">
                            <p className="text-gray-900 font-sans font-medium 
                            text-base not-italic leading-5 text-center">
                            Aloe
                            </p>
                            <div className="plantButtonDiv">
                                <p className="plantButtonText">
                                Review
                                </p>

                            </div>
                            
                        </div>

                    </div>


                    <div className="plantBox">
                        <div className="plantImgDiv">
                            <img className="h-20" src={require('../img/4.png')}/> 
                        </div> 
                        <div className="rating">
                            <img className=" h-3" src={require('../img/Star.png')}/> 
                             <p className="text-xs text-white rounded-sm">
                                 8.0
                            </p>    
                        </div>
                        <div className="w-full flex flex-col items-center justify-end p-[8px] gap-5">
                            <p className="text-gray-900 font-sans font-medium 
                            text-base not-italic leading-5 text-center">
                            Chtysanthemum
                            </p>
                            <div className="plantButtonDiv">
                                <p className="plantButtonText">
                                Review
                                </p>

                            </div>
                            
                        </div>

                    </div>

                   

                </div>

                    

                


            </div>

            <div className="p-0
            mx-[8px]  my-[36px] ">

                <div className="flex justify-between items-center py-[4px] px-[0px] w-[100%] ">
                    <p className="text-gray-900 font-sans font-bold text-lg not-italic leading-6 ">
                        Plant Care
                    </p>
                    <a className="plantButtonText">
                        See all
                    </a>
                    
                </div>

                <div className=" mt-[16px] flex flex-col overflow-x-scroll">
                <div className="flex items-start  p-0 gap-[16px] w-[100%]">
                    <div className="flex w-[256px] flex-col items-center p-[8px] shadow-md 
                    bg-white gap-[8px] rounded-lg">
                        <img className="h-[124px]" src={require('../img/plcr1.png')}/>
                            <div className="flex flex-col items-start">
                                <div className=" text-lime-700 font-sans font-normal text-base not-italic leading-5 text-center">
                                    <p>
                                        Posts for plants
                                    </p>
                                </div>
                                <div className="text-gray-900 font-sans font-normal text-sm not-italic leading-4 text-center">
                                    <p>
                                    Flower pot Ingreen, plastic marble 
                                    </p> 
                                </div>
                            </div>
                            <div className=" flex justify-center w-[224px] text-white rounded-lg bg-lime-600 my-[8px]  py-[4px] px-[32px]">
                                <p>
                                    Go over
                                </p>
 
                            </div>
                        
                    </div>

                    <div className="flex w-[256px] flex-col items-center p-[8px] shadow-md 
                    bg-white gap-[8px] rounded-lg">
                        <img className="h-[124px]" src={require('../img/cat3.png')}/>
                            <div className="flex flex-col items-start">
                                <div className=" text-lime-700 font-sans font-normal text-base not-italic leading-5 text-center">
                                    <p>
                                    Simon's Cat
                                    </p>
                                </div>
                                <div className="text-gray-900 font-sans font-normal text-sm not-italic leading-4 text-center">
                                    <p>
                                    Cute cat 
                                    </p> 
                                </div>
                            </div>
                            <div className=" flex justify-center w-[224px] text-white rounded-lg bg-lime-600 my-[8px]  py-[4px] px-[32px]">
                                <p>
                                    Go over
                                </p>
 
                            </div>
                        
                    </div>


                    <div className="flex w-[256px] flex-col items-center p-[8px] shadow-md 
                    bg-white gap-[8px] rounded-lg">
                        <img className="h-[124px]" src={require('../img/cat4.png')}/>
                            <div className="flex flex-col items-start">
                                <div className=" text-lime-700 font-sans font-normal text-base not-italic leading-5 text-center">
                                    <p>
                                        Cats
                                    </p>
                                </div>
                                <div className="text-gray-900 font-sans font-normal text-sm not-italic leading-4 text-center">
                                    <p>
                                    A lot of cats
                                    </p> 
                                </div>
                            </div>
                            <div className=" flex justify-center w-[224px] text-white rounded-lg bg-lime-600 my-[8px]  py-[4px] px-[32px]">
                                <p>
                                    Go over
                                </p>
 
                            </div>
                        
                    </div>







                </div>
                </div>


            </div>


            <div className=" flex justify-around items-center  px-[8px] h-[120px] bg-gray-900 m-0" >
            <img className="" src={require('../img/18.png')}/>
            <img className="" src={require('../img/Text.png')}/>
            </div>
            
        </div>
        
     </div>
}

export default App
